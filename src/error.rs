use {
    isahc::http::uri::InvalidUri,
    std::{error, fmt, io, str::Utf8Error},
};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("html scraping error: {err}")]
    Scrape {
        #[from]
        err: ScrapeError,
    },

    #[error("http request error: {err}")]
    Request {
        #[from]
        err: RequestError,
    },

    #[error("invalid url passed: {err}")]
    Uri {
        #[from]
        err: InvalidUri,
    },

    #[error("tokio thread pool is empty or missing")]
    Canceled,
}

#[derive(Debug)]
pub enum ScrapeError {
    Io(io::Error),
    Utf8(Utf8Error),

    ElementNotFound(&'static str, &'static str),
    AttributeNotFound(&'static str, &'static str),
    UnparsableDate(&'static str),
}

impl fmt::Display for ScrapeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ScrapeError::Io(ref err) => err.fmt(f),
            ScrapeError::Utf8(ref err) => err.fmt(f),
            ScrapeError::ElementNotFound(ref site, ref selector) => write!(
                f,
                "Sector element for site {} not found: {}",
                site, selector
            ),
            ScrapeError::AttributeNotFound(ref site, ref selector) => write!(
                f,
                "Element attribute for site {} not found: {}",
                site, selector
            ),
            ScrapeError::UnparsableDate(ref site) => {
                write!(f, "Unparsable date time for site {}", site,)
            }
        }
    }
}

impl error::Error for ScrapeError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            ScrapeError::Io(ref err) => Some(err),
            ScrapeError::Utf8(ref err) => Some(err),
            ScrapeError::ElementNotFound(_, _)
            | ScrapeError::AttributeNotFound(_, _)
            | ScrapeError::UnparsableDate(_) => None,
        }
    }
}

impl From<io::Error> for ScrapeError {
    fn from(err: io::Error) -> ScrapeError {
        ScrapeError::Io(err)
    }
}

impl From<Utf8Error> for ScrapeError {
    fn from(err: Utf8Error) -> ScrapeError {
        ScrapeError::Utf8(err)
    }
}

#[derive(Debug)]
pub enum RequestError {
    Http(isahc::http::Error),
    Io(io::Error),
    Isahc(isahc::Error),
}

impl fmt::Display for RequestError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            RequestError::Http(ref err) => err.fmt(f),
            RequestError::Io(ref err) => err.fmt(f),
            RequestError::Isahc(ref err) => err.fmt(f),
        }
    }
}

impl error::Error for RequestError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            RequestError::Http(ref err) => Some(err),
            RequestError::Io(ref err) => Some(err),
            RequestError::Isahc(ref err) => Some(err),
        }
    }
}

impl From<isahc::http::Error> for RequestError {
    fn from(err: isahc::http::Error) -> RequestError {
        RequestError::Http(err)
    }
}

impl From<io::Error> for RequestError {
    fn from(err: io::Error) -> RequestError {
        RequestError::Io(err)
    }
}

impl From<isahc::Error> for RequestError {
    fn from(err: isahc::Error) -> RequestError {
        RequestError::Isahc(err)
    }
}
