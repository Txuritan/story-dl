use {
    clap::{App, Arg},
    std::fs,
    story_dl::{
        archive_of_our_own, fanfiction,
        models::{Format, Story},
        Uri,
    },
};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(tracing::Level::INFO)
        .finish();

    tracing::subscriber::set_global_default(subscriber)?;

    tracing_log::LogTracer::init()?;

    let matches = App::new(clap::crate_name!())
        .author(clap::crate_authors!())
        .about(clap::crate_description!())
        .version(clap::crate_version!())
        .arg(
            Arg::with_name("file")
                .short("f")
                .value_name("FILE")
                .takes_value(true)
                .conflicts_with("url")
                .help("File to use as a scraping list"),
        )
        .arg(
            Arg::with_name("url")
                .short("u")
                .value_name("URL")
                .takes_value(true)
                .help("URL of the story to download"),
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .value_name("FORMAT")
                .possible_values(Format::variants().as_slice())
                .default_value("epub")
                .required(true)
                .help("Output format of the downloaded story"),
        )
        .get_matches();

    let output = matches
        .value_of("output")
        .unwrap()
        .parse::<Format>()
        .map_err(|_| anyhow::format_err!("valid values: [epub, json, message-pack]"))?;

    if matches.is_present("file") {
        let file = matches.value_of("file").unwrap();

        let imports = serde_json::from_slice::<Import>(&fs::read(file)?[..])?;

        for import in imports {
            let url: Uri = match import {
                Element::Text(url) => url,
                Element::Url { url } => url,
            }
            .parse()?;

            let story = scrape(&url).await?;

            let mut buffer = Vec::with_capacity(story.words as usize);

            to_buffer(&story, output, &mut buffer)?;

            fs::write(
                format!(
                    "{} - {}.{}",
                    story.name,
                    story.authors.join(", "),
                    output.file_extension()
                ),
                &buffer,
            )?;
        }
    } else if matches.is_present("url") {
        let url: Uri = matches.value_of("url").unwrap().parse()?;

        let story = scrape(&url).await?;

        let mut buffer = Vec::with_capacity(story.words as usize);

        to_buffer(&story, output, &mut buffer)?;

        fs::write(
            format!(
                "{} - {}.{}",
                story.name,
                story.authors.join(", "),
                output.file_extension()
            ),
            &buffer,
        )?;
    }

    Ok(())
}

async fn scrape(url: &Uri) -> anyhow::Result<Story> {
    let site = Site::from_url(url).expect("Given url doesn't match any known site");

    let story = match site {
        Site::ArchiveOfOurOwn => archive_of_our_own::scrape(url).await?,
        Site::FanFiction => fanfiction::scrape(url).await?,
    };

    Ok(story)
}

enum Site {
    ArchiveOfOurOwn,
    FanFiction,
}

impl Site {
    fn from_url(url: &Uri) -> Option<Self> {
        url.host().and_then(|host| match host {
            "archiveofourown.org" | "www.archiveofourown.org" => Some(Site::ArchiveOfOurOwn),
            "fanfiction.net" | "www.fanfiction.net" | "m.fanfiction.net" => Some(Site::FanFiction),
            _ => None,
        })
    }
}

type Import = Vec<Element>;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
#[serde(untagged)]
enum Element {
    Url { url: String },
    Text(String),
}
fn to_buffer(story: &Story, format: Format, mut buff: &mut [u8]) -> anyhow::Result<()> {
    to_writer(story, format, &mut buff)?;

    Ok(())
}

fn to_writer(
    story: &Story,
    format: Format,
    writer: &mut impl std::io::Write,
) -> anyhow::Result<()> {
    match format {
        #[cfg(feature = "epub")]
        Format::Epub => {
            use pulldown_cmark::{html, Options, Parser};

            let mut epub = epub::Builder::new()?;

            epub.metadata("title", &story.name)?;
            epub.metadata("author", story.authors.join(", "))?;
            epub.metadata("description", &story.summary)?;

            epub.inline_toc();

            let mut pre_buf = String::new();
            let mut main_buf = String::new();
            let mut post_buf = String::new();

            for (i, chapter) in story.chapters.iter().enumerate() {
                let pre_parser = Parser::new_ext(&chapter.pre, Options::all());
                html::push_html(&mut pre_buf, pre_parser);

                let main_parser = Parser::new_ext(&chapter.main, Options::all());
                html::push_html(&mut main_buf, main_parser);

                let post_parser = Parser::new_ext(&chapter.post, Options::all());
                html::push_html(&mut post_buf, post_parser);

                let body = format!(
                    r#"<?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="Content-Style-Type" content="text/css" />
      <meta name="generator" content="story-dl" />
      <title>{}</title>
      <link rel="stylesheet" type="text/css" href="stylesheet.css" />
    </head>
    <body>
      {pre}
      {sep}
      {main}
      {sep}
      {post}
    </body>
    </html>"#,
                    sep = "<br/><hr/><br/>",
                    pre = pre_buf,
                    main = main_buf,
                    post = post_buf,
                );

                epub.add_content(
                    &chapter.name,
                    format!("chapter-{}.xhtml", i + 1),
                    body.as_bytes(),
                )?;

                pre_buf.clear();
                main_buf.clear();
                post_buf.clear();
            }

            epub.generate(writer)?;
        }

        #[cfg(feature = "json")]
        Format::Json => {
            serde_json::to_writer(writer, story)?;
        }

        #[cfg(feature = "messagepack")]
        Format::MessagePack => {
            rmps::encode::write(writer, story)?;
        }
    }

    Ok(())
}

// TODO: implement epub v2
#[cfg(feature = "epub")]
mod epub {
    // This is a very stripped down and modified version of
    // https://github.com/lise-henry/epub-builder
    // I converted the template system to askama and are using anyhow for errors
    // this version only supports epub v2

    // LICENSE for epub-builder (v0.4) by Élisabeth Henry (https://github.com/lise-henry)
    // Mozilla Public License Version 2.0
    // ==================================
    //
    // 1. Definitions
    // --------------
    //
    // 1.1. "Contributor"
    //     means each individual or legal entity that creates, contributes to
    //     the creation of, or owns Covered Software.
    //
    // 1.2. "Contributor Version"
    //     means the combination of the Contributions of others (if any) used
    //     by a Contributor and that particular Contributor's Contribution.
    //
    // 1.3. "Contribution"
    //     means Covered Software of a particular Contributor.
    //
    // 1.4. "Covered Software"
    //     means Source Code Form to which the initial Contributor has attached
    //     the notice in Exhibit A, the Executable Form of such Source Code
    //     Form, and Modifications of such Source Code Form, in each case
    //     including portions thereof.
    //
    // 1.5. "Incompatible With Secondary Licenses"
    //     means
    //
    //     (a) that the initial Contributor has attached the notice described
    //         in Exhibit B to the Covered Software; or
    //
    //     (b) that the Covered Software was made available under the terms of
    //         version 1.1 or earlier of the License, but not also under the
    //         terms of a Secondary License.
    //
    // 1.6. "Executable Form"
    //     means any form of the work other than Source Code Form.
    //
    // 1.7. "Larger Work"
    //     means a work that combines Covered Software with other material, in
    //     a separate file or files, that is not Covered Software.
    //
    // 1.8. "License"
    //     means this document.
    //
    // 1.9. "Licensable"
    //     means having the right to grant, to the maximum extent possible,
    //     whether at the time of the initial grant or subsequently, any and
    //     all of the rights conveyed by this License.
    //
    // 1.10. "Modifications"
    //     means any of the following:
    //
    //     (a) any file in Source Code Form that results from an addition to,
    //         deletion from, or modification of the contents of Covered
    //         Software; or
    //
    //     (b) any new file in Source Code Form that contains any Covered
    //         Software.
    //
    // 1.11. "Patent Claims" of a Contributor
    //     means any patent claim(s), including without limitation, method,
    //     process, and apparatus claims, in any patent Licensable by such
    //     Contributor that would be infringed, but for the grant of the
    //     License, by the making, using, selling, offering for sale, having
    //     made, import, or transfer of either its Contributions or its
    //     Contributor Version.
    //
    // 1.12. "Secondary License"
    //     means either the GNU General Public License, Version 2.0, the GNU
    //     Lesser General Public License, Version 2.1, the GNU Affero General
    //     Public License, Version 3.0, or any later versions of those
    //     licenses.
    //
    // 1.13. "Source Code Form"
    //     means the form of the work preferred for making modifications.
    //
    // 1.14. "You" (or "Your")
    //     means an individual or a legal entity exercising rights under this
    //     License. For legal entities, "You" includes any entity that
    //     controls, is controlled by, or is under common control with You. For
    //     purposes of this definition, "control" means (a) the power, direct
    //     or indirect, to cause the direction or management of such entity,
    //     whether by contract or otherwise, or (b) ownership of more than
    //     fifty percent (50%) of the outstanding shares or beneficial
    //     ownership of such entity.
    //
    // 2. License Grants and Conditions
    // --------------------------------
    //
    // 2.1. Grants
    //
    // Each Contributor hereby grants You a world-wide, royalty-free,
    // non-exclusive license:
    //
    // (a) under intellectual property rights (other than patent or trademark)
    //     Licensable by such Contributor to use, reproduce, make available,
    //     modify, display, perform, distribute, and otherwise exploit its
    //     Contributions, either on an unmodified basis, with Modifications, or
    //     as part of a Larger Work; and
    //
    // (b) under Patent Claims of such Contributor to make, use, sell, offer
    //     for sale, have made, import, and otherwise transfer either its
    //     Contributions or its Contributor Version.
    //
    // 2.2. Effective Date
    //
    // The licenses granted in Section 2.1 with respect to any Contribution
    // become effective for each Contribution on the date the Contributor first
    // distributes such Contribution.
    //
    // 2.3. Limitations on Grant Scope
    //
    // The licenses granted in this Section 2 are the only rights granted under
    // this License. No additional rights or licenses will be implied from the
    // distribution or licensing of Covered Software under this License.
    // Notwithstanding Section 2.1(b) above, no patent license is granted by a
    // Contributor:
    //
    // (a) for any code that a Contributor has removed from Covered Software;
    //     or
    //
    // (b) for infringements caused by: (i) Your and any other third party's
    //     modifications of Covered Software, or (ii) the combination of its
    //     Contributions with other software (except as part of its Contributor
    //     Version); or
    //
    // (c) under Patent Claims infringed by Covered Software in the absence of
    //     its Contributions.
    //
    // This License does not grant any rights in the trademarks, service marks,
    // or logos of any Contributor (except as may be necessary to comply with
    // the notice requirements in Section 3.4).
    //
    // 2.4. Subsequent Licenses
    //
    // No Contributor makes additional grants as a result of Your choice to
    // distribute the Covered Software under a subsequent version of this
    // License (see Section 10.2) or under the terms of a Secondary License (if
    // permitted under the terms of Section 3.3).
    //
    // 2.5. Representation
    //
    // Each Contributor represents that the Contributor believes its
    // Contributions are its original creation(s) or it has sufficient rights
    // to grant the rights to its Contributions conveyed by this License.
    //
    // 2.6. Fair Use
    //
    // This License is not intended to limit any rights You have under
    // applicable copyright doctrines of fair use, fair dealing, or other
    // equivalents.
    //
    // 2.7. Conditions
    //
    // Sections 3.1, 3.2, 3.3, and 3.4 are conditions of the licenses granted
    // in Section 2.1.
    //
    // 3. Responsibilities
    // -------------------
    //
    // 3.1. Distribution of Source Form
    //
    // All distribution of Covered Software in Source Code Form, including any
    // Modifications that You create or to which You contribute, must be under
    // the terms of this License. You must inform recipients that the Source
    // Code Form of the Covered Software is governed by the terms of this
    // License, and how they can obtain a copy of this License. You may not
    // attempt to alter or restrict the recipients' rights in the Source Code
    // Form.
    //
    // 3.2. Distribution of Executable Form
    //
    // If You distribute Covered Software in Executable Form then:
    //
    // (a) such Covered Software must also be made available in Source Code
    //     Form, as described in Section 3.1, and You must inform recipients of
    //     the Executable Form how they can obtain a copy of such Source Code
    //     Form by reasonable means in a timely manner, at a charge no more
    //     than the cost of distribution to the recipient; and
    //
    // (b) You may distribute such Executable Form under the terms of this
    //     License, or sublicense it under different terms, provided that the
    //     license for the Executable Form does not attempt to limit or alter
    //     the recipients' rights in the Source Code Form under this License.
    //
    // 3.3. Distribution of a Larger Work
    //
    // You may create and distribute a Larger Work under terms of Your choice,
    // provided that You also comply with the requirements of this License for
    // the Covered Software. If the Larger Work is a combination of Covered
    // Software with a work governed by one or more Secondary Licenses, and the
    // Covered Software is not Incompatible With Secondary Licenses, this
    // License permits You to additionally distribute such Covered Software
    // under the terms of such Secondary License(s), so that the recipient of
    // the Larger Work may, at their option, further distribute the Covered
    // Software under the terms of either this License or such Secondary
    // License(s).
    //
    // 3.4. Notices
    //
    // You may not remove or alter the substance of any license notices
    // (including copyright notices, patent notices, disclaimers of warranty,
    // or limitations of liability) contained within the Source Code Form of
    // the Covered Software, except that You may alter any license notices to
    // the extent required to remedy known factual inaccuracies.
    //
    // 3.5. Application of Additional Terms
    //
    // You may choose to offer, and to charge a fee for, warranty, support,
    // indemnity or liability obligations to one or more recipients of Covered
    // Software. However, You may do so only on Your own behalf, and not on
    // behalf of any Contributor. You must make it absolutely clear that any
    // such warranty, support, indemnity, or liability obligation is offered by
    // You alone, and You hereby agree to indemnify every Contributor for any
    // liability incurred by such Contributor as a result of warranty, support,
    // indemnity or liability terms You offer. You may include additional
    // disclaimers of warranty and limitations of liability specific to any
    // jurisdiction.
    //
    // 4. Inability to Comply Due to Statute or Regulation
    // ---------------------------------------------------
    //
    // If it is impossible for You to comply with any of the terms of this
    // License with respect to some or all of the Covered Software due to
    // statute, judicial order, or regulation then You must: (a) comply with
    // the terms of this License to the maximum extent possible; and (b)
    // describe the limitations and the code they affect. Such description must
    // be placed in a text file included with all distributions of the Covered
    // Software under this License. Except to the extent prohibited by statute
    // or regulation, such description must be sufficiently detailed for a
    // recipient of ordinary skill to be able to understand it.
    //
    // 5. Termination
    // --------------
    //
    // 5.1. The rights granted under this License will terminate automatically
    // if You fail to comply with any of its terms. However, if You become
    // compliant, then the rights granted under this License from a particular
    // Contributor are reinstated (a) provisionally, unless and until such
    // Contributor explicitly and finally terminates Your grants, and (b) on an
    // ongoing basis, if such Contributor fails to notify You of the
    // non-compliance by some reasonable means prior to 60 days after You have
    // come back into compliance. Moreover, Your grants from a particular
    // Contributor are reinstated on an ongoing basis if such Contributor
    // notifies You of the non-compliance by some reasonable means, this is the
    // first time You have received notice of non-compliance with this License
    // from such Contributor, and You become compliant prior to 30 days after
    // Your receipt of the notice.
    //
    // 5.2. If You initiate litigation against any entity by asserting a patent
    // infringement claim (excluding declaratory judgment actions,
    // counter-claims, and cross-claims) alleging that a Contributor Version
    // directly or indirectly infringes any patent, then the rights granted to
    // You by any and all Contributors for the Covered Software under Section
    // 2.1 of this License shall terminate.
    //
    // 5.3. In the event of termination under Sections 5.1 or 5.2 above, all
    // end user license agreements (excluding distributors and resellers) which
    // have been validly granted by You or Your distributors under this License
    // prior to termination shall survive termination.
    //
    // ************************************************************************
    // *                                                                      *
    // *  6. Disclaimer of Warranty                                           *
    // *  -------------------------                                           *
    // *                                                                      *
    // *  Covered Software is provided under this License on an "as is"       *
    // *  basis, without warranty of any kind, either expressed, implied, or  *
    // *  statutory, including, without limitation, warranties that the       *
    // *  Covered Software is free of defects, merchantable, fit for a        *
    // *  particular purpose or non-infringing. The entire risk as to the     *
    // *  quality and performance of the Covered Software is with You.        *
    // *  Should any Covered Software prove defective in any respect, You     *
    // *  (not any Contributor) assume the cost of any necessary servicing,   *
    // *  repair, or correction. This disclaimer of warranty constitutes an   *
    // *  essential part of this License. No use of any Covered Software is   *
    // *  authorized under this License except under this disclaimer.         *
    // *                                                                      *
    // ************************************************************************
    //
    // ************************************************************************
    // *                                                                      *
    // *  7. Limitation of Liability                                          *
    // *  --------------------------                                          *
    // *                                                                      *
    // *  Under no circumstances and under no legal theory, whether tort      *
    // *  (including negligence), contract, or otherwise, shall any           *
    // *  Contributor, or anyone who distributes Covered Software as          *
    // *  permitted above, be liable to You for any direct, indirect,         *
    // *  special, incidental, or consequential damages of any character      *
    // *  including, without limitation, damages for lost profits, loss of    *
    // *  goodwill, work stoppage, computer failure or malfunction, or any    *
    // *  and all other commercial damages or losses, even if such party      *
    // *  shall have been informed of the possibility of such damages. This   *
    // *  limitation of liability shall not apply to liability for death or   *
    // *  personal injury resulting from such party's negligence to the       *
    // *  extent applicable law prohibits such limitation. Some               *
    // *  jurisdictions do not allow the exclusion or limitation of           *
    // *  incidental or consequential damages, so this exclusion and          *
    // *  limitation may not apply to You.                                    *
    // *                                                                      *
    // ************************************************************************
    //
    // 8. Litigation
    // -------------
    //
    // Any litigation relating to this License may be brought only in the
    // courts of a jurisdiction where the defendant maintains its principal
    // place of business and such litigation shall be governed by laws of that
    // jurisdiction, without reference to its conflict-of-law provisions.
    // Nothing in this Section shall prevent a party's ability to bring
    // cross-claims or counter-claims.
    //
    // 9. Miscellaneous
    // ----------------
    //
    // This License represents the complete agreement concerning the subject
    // matter hereof. If any provision of this License is held to be
    // unenforceable, such provision shall be reformed only to the extent
    // necessary to make it enforceable. Any law or regulation which provides
    // that the language of a contract shall be construed against the drafter
    // shall not be used to construe this License against a Contributor.
    //
    // 10. Versions of the License
    // ---------------------------
    //
    // 10.1. New Versions
    //
    // Mozilla Foundation is the license steward. Except as provided in Section
    // 10.3, no one other than the license steward has the right to modify or
    // publish new versions of this License. Each version will be given a
    // distinguishing version number.
    //
    // 10.2. Effect of New Versions
    //
    // You may distribute the Covered Software under the terms of the version
    // of the License under which You originally received the Covered Software,
    // or under the terms of any subsequent version published by the license
    // steward.
    //
    // 10.3. Modified Versions
    //
    // If you create software not governed by this License, and you want to
    // create a new license for such software, you may create and use a
    // modified version of this License if you rename the license and remove
    // any references to the name of the license steward (except to note that
    // such modified license differs from this License).
    //
    // 10.4. Distributing Source Code Form that is Incompatible With Secondary
    // Licenses
    //
    // If You choose to distribute Source Code Form that is Incompatible With
    // Secondary Licenses under the terms of this version of the License, the
    // notice described in Exhibit B of this License must be attached.
    //
    // Exhibit A - Source Code Form License Notice
    // -------------------------------------------
    //
    //   This Source Code Form is subject to the terms of the Mozilla Public
    //   License, v. 2.0. If a copy of the MPL was not distributed with this
    //   file, You can obtain one at http://mozilla.org/MPL/2.0/.
    //
    // If it is not possible or desirable to put the notice in a particular
    // file, then You may include the notice in a location (such as a LICENSE
    // file in a relevant directory) where a recipient would be likely to look
    // for such a notice.
    //
    // You may add additional accurate notices of copyright ownership.
    //
    // Exhibit B - "Incompatible With Secondary Licenses" Notice
    // ---------------------------------------------------------
    //
    //   This Source Code Form is "Incompatible With Secondary Licenses", as
    //   defined by the Mozilla Public License, v. 2.0.

    use {
        askama::Template,
        std::{
            fmt::Write as _,
            io::{self, Cursor, Read, Write},
            path::Path,
        },
    };

    pub struct Builder {
        archive: zip::ZipWriter<Cursor<Vec<u8>>>,
        metadata: Metadata,
        toc: Toc,
        files: Vec<Content>,
    }

    impl Builder {
        pub fn new() -> anyhow::Result<Self> {
            let mut this = Self {
                archive: zip::ZipWriter::new(Cursor::new(Vec::new())),
                metadata: Metadata::new(),
                toc: Toc::new(),
                files: vec![],
            };

            this.add_file(
                "META-INF/container.xml",
                templates::Container {}.render()?.as_bytes(),
            )?;
            this.add_file(
                "META-INF/com.apple.ibooks.display-options.xml",
                templates::IBooks {}.render()?.as_bytes(),
            )?;

            Ok(this)
        }

        pub fn metadata(
            &mut self,
            key: impl AsRef<str>,
            value: impl Into<String>,
        ) -> anyhow::Result<()> {
            match key.as_ref() {
                "author" => self.metadata.author = value.into(),
                "title" => self.metadata.title = value.into(),
                "lang" => self.metadata.lang = value.into(),
                "generator" => self.metadata.generator = value.into(),
                "description" => self.metadata.description = Some(value.into()),
                "subject" => self.metadata.subject = Some(value.into()),
                "license" => self.metadata.license = Some(value.into()),
                "toc_name" => self.metadata.toc_name = value.into(),
                s => anyhow::bail!("invalid metadata '{}'", s),
            }
            Ok(())
        }

        pub fn inline_toc(&mut self) -> &mut Self {
            self.toc.add(TocElement::new(
                "toc.xhtml",
                self.metadata.toc_name.as_str(),
            ));

            let mut file = Content::new("toc.xhtml", "application/xhtml+xml");

            file.reftype = ReferenceType::Toc;
            file.title = self.metadata.toc_name.clone();
            file.itemref = true;

            self.files.push(file);

            self
        }

        pub fn add_content(
            &mut self,
            title: impl Into<String>,
            file: impl Into<String>,
            content: impl Read,
        ) -> anyhow::Result<()> {
            let file = file.into();

            let toc_element = TocElement::new(file, title);

            self.add_file(Path::new("OEBPS").join(toc_element.path.as_str()), content)?;

            let mut file = Content::new(toc_element.path.as_str(), "application/xhtml+xml");

            file.itemref = true;

            self.files.push(file);

            if !toc_element.title.is_empty() {
                self.toc.add(toc_element);
            }

            Ok(())
        }

        pub fn generate(&mut self, mut file_name: impl Write) -> anyhow::Result<()> {
            // render content
            let mut optional = String::new();

            if let Some(desc) = &self.metadata.description {
                writeln!(optional, "<dc:description>{}</dc:description>", desc)?;
            }

            let mut items = String::new();
            let mut itemrefs = String::new();
            let mut guide = String::new();

            for file in &self.files {
                let id = file.file.replace(".", "_").replace("/", "_");

                writeln!(
                    items,
                    r#"<item media-type="{}" id="{}" href="{}" />"#,
                    file.mime, id, file.file
                )?;

                if file.itemref {
                    writeln!(itemrefs, r#"<itemref idref=\"{}\" />"#, id)?;
                }

                writeln!(
                    guide,
                    r#"<reference type="{}" title="{}" href="{}" />"#,
                    match file.reftype {
                        ReferenceType::Text => "text",
                        ReferenceType::Toc => "toc",
                    },
                    file.title.replace('"', "&quot;"),
                    file.file
                )?;
            }

            self.add_file(
                "OEBPS/content.opf",
                templates::Content {
                    uuid: uuid::adapter::Urn::from_uuid(uuid::Uuid::new_v4()),
                    title: self.metadata.title.as_str(),
                    date: chrono::Utc::now().format("%Y-%m-%dT%H:%M:%SZ"),
                    lang: self.metadata.lang.as_str(),
                    author: self.metadata.author.as_str(),
                    toc_name: self.metadata.toc_name.as_str(),
                    optional,
                    items,
                    itemrefs,
                    guide,
                }
                .render()?
                .as_bytes(),
            )?;

            // render toc
            let rendered = self.toc.render_epub()?;
            self.add_file(
                "OEBPS/toc.ncx",
                templates::TableOfContents {
                    name: &self.metadata.toc_name,
                    points: rendered.as_str(),
                }
                .render()?
                .as_bytes(),
            )?;

            // render nav
            let rendered = self.render_nav(true)?;
            self.add_file("OEBPS/nav.xhtml", rendered.as_slice())?;

            // render toc
            let rendered = self.render_nav(true)?;
            self.add_file("OEBPS/toc.xhtml", rendered.as_slice())?;

            // write to zip
            let cursor = self.archive.finish()?;

            let bytes = cursor.into_inner();

            file_name.write_all(bytes.as_ref())?;

            Ok(())
        }

        fn add_file(
            &mut self,
            path: impl AsRef<Path>,
            mut content: impl Read,
        ) -> anyhow::Result<()> {
            let mut file = format!("{}", path.as_ref().display());

            if cfg!(target_os = "windows") {
                file = file.replace('\\', "/");
            }

            let options = zip::write::FileOptions::default();

            self.archive.start_file(file, options)?;

            io::copy(&mut content, &mut self.archive)?;

            Ok(())
        }

        fn render_nav(&mut self, numbered: bool) -> anyhow::Result<Vec<u8>> {
            let content = self.toc.render(numbered);

            let rendered = templates::Nav {
                content,
                toc_name: self.metadata.toc_name.as_str(),
                generator: self.metadata.generator.as_str(),
            }
            .render()?
            .into();

            Ok(rendered)
        }
    }

    enum ReferenceType {
        Text,
        Toc,
    }

    struct Metadata {
        title: String,
        author: String,
        lang: String,
        generator: String,
        toc_name: String,
        description: Option<String>,
        subject: Option<String>,
        license: Option<String>,
    }

    impl Metadata {
        fn new() -> Self {
            Self {
                title: String::new(),
                author: String::new(),
                lang: String::from("en"),
                generator: String::from("story-dl EPub Builder"),
                toc_name: String::from("Table Of Contents"),
                description: None,
                subject: None,
                license: None,
            }
        }
    }

    struct Toc {
        elements: Vec<TocElement>,
    }

    impl Toc {
        fn new() -> Self {
            Self { elements: vec![] }
        }

        fn add(&mut self, element: TocElement) -> &mut Self {
            let mut inserted = false;

            if let Some(last_elem) = self.elements.last_mut() {
                if element.level > last_elem.level {
                    last_elem.add(element.clone());
                    inserted = true;
                }
            }

            if !inserted {
                self.elements.push(element);
            }

            self
        }

        fn render_epub(&mut self) -> anyhow::Result<String> {
            let mut buff = Vec::new();

            let mut offset = 0;

            for element in &self.elements {
                let (o, r) = element.render_epub(offset)?;

                offset = o;

                writeln!(&mut buff, "{}", r)?;
            }

            Ok(String::from_utf8(buff)?)
        }

        fn render(&mut self, numbered: bool) -> String {
            let mut output = String::new();

            for elem in &self.elements {
                output.push_str(&elem.render(numbered));
            }

            format!(
                "<{oul}>\n{output}\n</{oul}>\n",
                output = output,
                oul = if numbered { "ol" } else { "ul" }
            )
        }
    }

    #[derive(Clone)]
    struct TocElement {
        level: usize,
        path: String,
        title: String,
        children: Vec<TocElement>,
    }

    impl TocElement {
        fn new(path: impl Into<String>, title: impl Into<String>) -> Self {
            Self {
                level: 1,
                path: path.into(),
                title: title.into(),
                children: vec![],
            }
        }

        fn add(&mut self, element: TocElement) {
            let mut inserted = false;

            if let Some(last_elem) = self.children.last_mut() {
                if element.level > last_elem.level {
                    last_elem.add(element.clone());
                    inserted = true;
                }
            }

            if !inserted {
                self.children.push(element);
            }
        }

        fn render_epub(&self, mut offset: usize) -> anyhow::Result<(usize, String)> {
            offset += 1;

            let mut buff = Vec::new();

            writeln!(&mut buff, r#"<navPoint id="navPoint-{}">"#, offset)?;

            writeln!(&mut buff, "<navLabel>")?;
            writeln!(&mut buff, "{}", self.title)?;
            writeln!(&mut buff, "</navLabel>")?;

            writeln!(&mut buff, r#"<content src="{}" />"#, self.path)?;

            if !self.children.is_empty() {
                for child in &self.children {
                    let (o, s) = child.render_epub(offset)?;

                    offset = o;

                    writeln!(&mut buff, "{}", s)?;
                }
            }

            writeln!(&mut buff, "</navPoint>")?;

            Ok((offset, String::from_utf8(buff)?))
        }

        fn render(&self, numbered: bool) -> String {
            if self.title.is_empty() {
                return String::new();
            }

            let children = if self.children.is_empty() {
                String::new()
            } else {
                let mut output = String::new();

                for child in &self.children {
                    output.push_str(&child.render(numbered));
                }

                format!(
                    "\n<{oul}>{children}\n</{oul}>\n",
                    oul = if numbered { "ol" } else { "ul" },
                    children = output
                )
            };

            format!(
                "<li><a href=\"{}\">{}</a>{}</li>\n",
                self.path, self.title, children,
            )
        }
    }

    struct Content {
        file: String,
        mime: String,
        reftype: ReferenceType,
        itemref: bool,
        title: String,
    }

    impl Content {
        fn new(file: impl Into<String>, mime: impl Into<String>) -> Self {
            Self {
                file: file.into(),
                mime: mime.into(),
                reftype: ReferenceType::Text,
                itemref: false,
                title: String::new(),
            }
        }
    }

    pub mod templates {
        #[derive(askama::Template)]
        #[template(path = "container.xml", escape = "none")]
        pub struct Container {}

        #[derive(askama::Template)]
        #[template(path = "ibooks.xml", escape = "none")]
        pub struct IBooks {}

        #[derive(askama::Template)]
        #[template(path = "toc.ncx", escape = "none")]
        pub struct TableOfContents<'t> {
            pub name: &'t str,
            pub points: &'t str,
        }

        #[derive(askama::Template)]
        #[template(path = "nav.xhtml", escape = "none")]
        pub struct Nav<'t> {
            pub content: String,
            pub toc_name: &'t str,
            pub generator: &'t str,
        }

        #[derive(askama::Template)]
        #[template(path = "content.opf", escape = "none")]
        pub struct Content<'t> {
            pub uuid: uuid::adapter::Urn,
            pub title: &'t str,
            pub date: chrono::format::DelayedFormat<chrono::format::strftime::StrftimeItems<'t>>,
            pub lang: &'t str,
            pub author: &'t str,
            pub toc_name: &'t str,
            pub optional: String,
            pub items: String,
            pub itemrefs: String,
            pub guide: String,
        }
    }
}
