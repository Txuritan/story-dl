pub mod archive_of_our_own;
pub mod fanfiction;

use {
    crate::{
        models::{Chapter, Details},
        utils::req,
        Error, Uri,
    },
    std::{fmt, sync::Arc},
};

#[macro_export]
macro_rules! select {
    (inner_html <> $site:expr; $html:expr => $selector:expr) => {
        $html
            .select($selector)
            .first()
            .and_then(|sd| sd.inner_html())
            .ok_or(crate::error::ScrapeError::ElementNotFound($site, $selector))
    };
    (string <> $site:expr; $html:expr => $selector:expr) => {
        $html
            .select($selector)
            .first()
            .and_then(|sd| sd.text())
            .ok_or(crate::error::ScrapeError::ElementNotFound($site, $selector))
    };
    (string[] <> $site:expr; $html:expr => $selector:expr) => {
        $html
            .select($selector)
            .into_iter()
            .map(|ele| ele.text())
            .collect::<Option<Vec<_>>>()
            .ok_or(crate::error::ScrapeError::ElementNotFound($site, $selector))
    };
}

#[derive(Clone, Copy, Debug, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub enum Sites {
    ArchiveOfOurOwn,
    FanFictionNet,
}

impl Sites {
    pub fn url(&self) -> &'static str {
        match self {
            Sites::ArchiveOfOurOwn => "https://archiveofourown.org/",
            Sites::FanFictionNet => "https://fanfiction.net/",
        }
    }
}

impl fmt::Display for Sites {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Sites::ArchiveOfOurOwn => write!(f, "Archive of Our Own"),
            Sites::FanFictionNet => write!(f, "FanFiction.net"),
        }
    }
}

pub struct SiteRef {
    inner: Inner,
}

impl SiteRef {
    pub async fn get_details(&mut self) -> Result<Details, Error> {
        match &mut self.inner {
            Inner::ArchiveOfOurOwn { id, document } => {
                if document.is_none() {
                    let url = format!(
                        "https://archiveofourown.org/works/{}?view_full_work=true",
                        id
                    )
                    .parse::<Uri>()?;

                    let body = req(&url).await?;

                    *document = Some(Arc::new(body));
                }

                let document = document.clone().expect("This should not be `None`");

                let details =
                    tokio::task::spawn_blocking(|| archive_of_our_own::get_details(document))
                        .await
                        .expect("Thread pool closed")?;

                Ok(details)
            }
            Inner::FanFictionNet { id } => {
                let url = format!("https://www.fanfiction.net/s/{}/{}", id, 1).parse::<Uri>()?;

                let body = req(&url).await?;

                let details = tokio::task::spawn_blocking(|| fanfiction::get_details(body))
                    .await
                    .expect("Thread pool closed")?;

                Ok(details)
            }
        }
    }

    pub async fn get_chapter(&mut self, chapter: u32) -> Result<Chapter, Error> {
        match &mut self.inner {
            Inner::ArchiveOfOurOwn { id, document } => {
                if document.is_none() {
                    let url = format!(
                        "https://archiveofourown.org/works/{}?view_full_work=true",
                        id
                    )
                    .parse::<Uri>()?;

                    let body = req(&url).await?;

                    *document = Some(Arc::new(body));
                }

                todo!()
            }
            Inner::FanFictionNet { id } => {
                let url =
                    format!("https://www.fanfiction.net/s/{}/{}", id, chapter).parse::<Uri>()?;

                let body = req(&url).await?;

                let chapter = tokio::task::spawn_blocking(|| fanfiction::get_chapter(body))
                    .await
                    .expect("Thread pool closed")?;

                Ok(chapter)
            }
        }
    }
}

enum Inner {
    ArchiveOfOurOwn {
        id: String,
        document: Option<Arc<String>>,
    },
    FanFictionNet {
        id: String,
    },
}

pub trait Site: Copy {
    fn init(self, id: impl Into<String>) -> SiteRef;
}

impl Site for Sites {
    fn init(self, id: impl Into<String>) -> SiteRef {
        match self {
            Sites::ArchiveOfOurOwn => SiteRef {
                inner: Inner::ArchiveOfOurOwn {
                    id: id.into(),
                    document: None,
                },
            },
            Sites::FanFictionNet => SiteRef {
                inner: Inner::FanFictionNet { id: id.into() },
            },
        }
    }
}
